Task 1:

import kotlinx.coroutines.*

fun main() = runBlocking {
    launch {
        delay(1000L)
        println("World")
    }
    println("Hello,")
    delay(2000L)
}

Task 2:

import kotlinx.coroutines.*

suspend fun firstNumber(): Int {
    delay(1000L)
    return 10
}

suspend fun secondNumber(): Int {
    delay(2000L)
    return 20
}

fun main() = runBlocking {
    val startTime = System.currentTimeMillis()
    val result1 = firstNumber() + secondNumber()
    println("Sequential result: $result1, time: ${System.currentTimeMillis() - startTime} ms")

    val deferred1 = async { firstNumber() }
    val deferred2 = async { secondNumber() }
    val result2 = deferred1.await() + deferred2.await()
    println("Concurrent result: $result2, time: ${System.currentTimeMillis() - startTime} ms")
}

Task 3:

import kotlinx.coroutines.*

fun main() = runBlocking {
    repeat(3) {
        launch {
            delay((it + 1) * 1000L)
            println("I'm sleeping $it...")
        }
    }
    println("main: I'm tired of waiting!")
    coroutineContext.cancelChildren()
    println("main: Now I can quit.")
}
