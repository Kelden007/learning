import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
 
class MainActivity : AppCompatActivity() {
 
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
 
        button_next.setOnClickListener {
            val intent = Intent(this@MainActivity, InfoActivity::class.java)
            intent.putExtra("surname", edit_surname.text.toString())
            intent.putExtra("name", edit_name.text.toString())
            intent.putExtra("patronymic", edit_patronymic.text.toString())
            intent.putExtra("age", edit_age.text.toString())
            intent.putExtra("hobby", edit_hobby.text.toString())
            startActivity(intent)
        }
    }
}
