class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button)
        val imageView = findViewById<ImageView>(R.id.imageView)

        button.setOnClickListener {
            val image = when (Random.nextInt(4)) {
                0 -> R.drawable.image1
                1 -> R.drawable.image2
                2 -> R.drawable.image3
                else -> R.drawable.image4
            }
            imageView.setImageResource(image)
            button.setBackgroundColor(Color.rgb(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256)))
        }
    }
}
